## Getting Started

First setup Tokens and email in your local environment

**_NOTE:_** When you first create your tokens you will not be able to retrieve them again. I chose to store them in my heb personal 1passwordvault for now.

```bash
# Setup Atlassian Token
# Create Token at https://id.atlassian.com/manage-profile/security/api-tokens
export ATLASTOKEN=[Your Access Token]
# Setup SourceGraph Token
# Create Token at https://heb.sourcegraph.com/users/[Lastname.Firstname]/settings/tokens
export SOURCEGTOKEN=[Your Access Token]
# Setup Email
export EMAIL=[Lastname.firstname@heb.com]
```

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:4000](http://localhost:4000) with your browser to view sourcegraph ruru ui.

You can start editing the server by modifying `server.js`.

## Future Enhancements

[Megamind Confluence Doc](https://hebecom.atlassian.net/wiki/x/4oJe5Q)
