var express = require("express");
var { createHandler } = require("graphql-http/lib/use/express");
var { buildSchema } = require("graphql");
var { ruruHTML } = require("ruru/server");

var cors = require("cors");

const JIRA_URL = "https://hebecom.atlassian.net/rest/api/3/search";
const CONFLUENCE_URL =
  "https://hebecom.atlassian.net/wiki/rest/api/content/search?";
const SOURCEGRAPH_URL = "https://heb.sourcegraph.com/.api/graphql";

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    jira(search: String!): Jira
    confluence(search: String!): Confluence
    sourceGraph(search: String!): SourceGraph
  }
  type Repository {
    url: String
    description: String
  }
  type SourceGraphSearch {
    results: SearchResult
  }
  type SourceGraphData {
    search: SourceGraphSearch
  }
  type SourceGraph {
    data: SourceGraphData
  }
  type SearchResult {
    repositories: [Repository]!
  }
  type Jira {
    issues: [Issue]
  }
  type JiraDescription {
    content: [JiraDescriptionContent]!
  }
  type JiraDescriptionContent {
    type: String
    content: [JiraTextContent]!
  }
  type JiraTextContent {
    type: String
    text: String
  }
  type Issue {
    key: String
    fields: Field
  }
  type Field {
    description: JiraDescription
  }
  type Confluence {
    results: [Document]
  }
  type Document {
    type: String
    title: String
    _expandable: DocumentData
    _links: DocumentLinks
  }
  type DocumentData {
    space: String
  }
  type DocumentLinks {
    webui: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  async jira(input) {
    const token = getAtlassianToken();
    const user = getEmail();
    const sanitizedInput = inputSanitizer(input.search);

    if (token && user) {
      const payload = {
        jql: `text~'${sanitizedInput}' order by created DESC`,
        maxResults: 20,
      };
      const response = await fetch(JIRA_URL, {
        method: "POST",
        headers: {
          Authorization: generateBasicAuth(user, token),
          "Content-Type": "application/json",
        },
        body: JSON.stringify(payload),
      });
      const data = await response.json();

      return data;
    } else {
      if (token) {
        console.log("jira:" + TOKEN_ERROR);
      } else {
        console.log("User email not set");
      }

      return "error";
    }
  },
  async confluence(input) {
    const token = getAtlassianToken();
    const user = getEmail();
    const sanitizedInput = inputSanitizer(input.search);
    const paramString = `text~${sanitizedInput} order by created DESC`;

    if (token && user) {
      const response = await fetch(
        CONFLUENCE_URL +
          new URLSearchParams({
            cql: paramString,
          }),
        {
          method: "GET",
          headers: {
            Authorization: generateBasicAuth(user, token),
            "Content-Type": "application/json",
          },
        }
      );
      const data = await response.json();

      return data;
    } else {
      if (token) {
        console.log("confluence:" + TOKEN_ERROR);
      } else {
        console.log("User email not set");
      }
    }
  },
  async sourceGraph(input) {
    const token = getSourceGraphToken();
    const sanitizedInput = inputSanitizer(input.search);
    const query = JSON.stringify({
      query: `query { search(patternType:standard query: \"${sanitizedInput}\") { results { dynamicFilters { kind } repositories { name, description, url } results { ... on CommitSearchResult { matches { body { text } } } } } } }`,
    });

    if (token) {
      const requestObj = {
        method: "POST",
        headers: {
          Authorization: getTokenAuth(token),
          "Content-Type": "application/json",
        },
        body: query,
      };
      const response = await fetch(SOURCEGRAPH_URL, requestObj);

      const data = await response.json();

      return data;
    } else {
      console.log("sourceGraph:" + TOKEN_ERROR);
      return "error";
    }
  },
};

var app = express();

const corsOptions = {
  origin: ["http://localhost:3000"],
};

app.use(cors(corsOptions));

// Create and use the GraphQL handler.
app.all(
  "/graphql",
  createHandler({
    schema: schema,
    rootValue: root,
  })
);

// Serve the GraphiQL IDE.
app.get("/", (_req, res) => {
  res.type("html");
  res.end(ruruHTML({ endpoint: "/graphql" }));
});

// Start the server at port
app.listen(4000);
console.log("Running a GraphQL API server at http://localhost:4000/graphql");

const getAtlassianToken = () => {
  return process.env.ATLASTOKEN;
};

const getSourceGraphToken = () => {
  return process.env.SOURCEGTOKEN;
};

const getEmail = () => {
  return process.env.EMAIL;
};

const generateBasicAuth = (username, token) => {
  return `Basic ${Buffer.from(username + ":" + token).toString("base64")}`;
};

const getTokenAuth = (token) => {
  return `token ${token}`;
};

const inputSanitizer = (input) => {
  input = input.replace(/[^a-z0-9 \.,_-]/gim, "");
  return input.trim();
};

const TOKEN_ERROR = "Invalid User Token";
